# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

# need for %.sort.cov
context prj/illumina_preprocessing

# couple of paired-sequences
# to be demultiplexed.
# Can be selected with
# $(wildcard ) function
PAIRED_FQ_GZ  ?=$(wildcard ../../seq/100000.?.fq.gz)

# bwa indexes
INDEXES ?= /genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta

# rad groups to be 
# associated to the alignments
RG ?= @RG\tID:kZAnw5bi8K\tPL:ILLUMINA\tCN:Istituto di Genomica Applicata\tDS:IGA-HiSeq2000-hiseq2-a-FC0171-CTTGTA\tDT:2012-06-29\tPG:BWA-0.7.5a\tPU:D0WR1ACXX\tLB:Sangiovese_VCR23_xMaitPair\tSM:Sangiovese_VCR23

# minimum insert size for
# mates to be selected
MATES_MIN_INSIZE ?= 678

# explicitly sets the Histogram width,
# overriding automatic truncation of
# histogram tail. 
# Default value: null
HISTOGRAM_WIDTH ?= null

logs:
	mkdir -p $@

fq.1.gz:
	ln -sf $(call first,$(PAIRED_FQ_GZ)) $@

fq.2.gz:
	ln -sf $(call last,$(PAIRED_FQ_GZ)) $@

#.PRECIOUS: aln.1.sai aln.2.sai
aln.%.sai: logs $(INDEXES) fq.%.gz
	!threads
	$(call load_modules); \
	bwa aln \
	-t $$THREADNUM \
	$^2 \
	$^3 \
	>$@ \
	2>$</bwa-aln.$@.log

aln.sam: logs $(INDEXES) aln.1.sai aln.2.sai fq.1.gz fq.2.gz
	$(call load_modules); \
	bwa sampe \
	-r '$(RG)' \
	$^2 \
	$^3 $^4 \
	$^5 $^6 \
	>$@ \
	2>$</bwa-sampe.$@.log

.PRECIOUS: aln.sort.bam
aln.sort.bam: logs aln.sam
	!threads
	$(call load_modules); \
	samtools view \
	-bS $^2 \
	2>$</samtools-view.$@.log \
	| samtools sort \
	-@ $$THREADNUM \
	- $(basename $@) \
	2>$</samtools-sort.$@.log
	
# aln.sort.bam: logs aln.sam
	# $(call load_modules); \
	# java -jar /iga/stratocluster/packages/sw/bio/picard-tools/1.124/picard.jar \
	# SortSam \
	# INPUT=$^2 \
	# OUTPUT=$@ \
	# SORT_ORDER=coordinate \
	# 3>&1 1>&2 2>&3 \
	# | tee $</picard-SortSam.$@.log


mates.sort.bam: logs aln.sort.bam
	!threads
	$(call load_modules); \
	samtools view -h $^2 \
	| bawk 'function abs(x){return ((x < 0.0) ? -x : x)} \
	!/^[\#,$$]/ { \
	if ( abs($$9) > $(MATES_MIN_INSIZE) || $$9 == 0 || $$1 ~ /@/ ) \
	print $$0; \
	else \
	print $$0 >"paired.sam"; }' \
	| samtools view -bS - \
	| samtools sort \
	-@ $$THREADNUM \
	- $(basename $@) \
	2>$</samtools-sort.$@.log

paired.sam: mates.sort.bam
	touch $@

paired.sort.bam: logs paired.sam aln.sort.bam
	!threads
	$(call load_modules); \
	paste -s -d "\n" \
	<(samtools view -H $^3) \
	<(cat $^2) \
	| samtools view -bS - \
	| samtools sort \
	-@ $$THREADNUM \
	- $(basename $@) \
	2>$</samtools-sort.$@.log


# %.uniq.sort.bam: logs %.sort.bam
# 	!threads
# 	$(call load_modules); \
# 	samtools view \
# 	-bhq 10 \
# 	-F '0x0100' \   * skip alignment that are not primary *
# 	$^2 \
# 	| samtools sort \
# 	-@ $$THREADNUM \
# 	- $(basename $@) \
# 	2>$</samtools-sort.$@.log


%.sort.insert_size.txt: logs $(INDEXES) %.sort.bam
	module load sw/bio/picard-tools/1.88; \
	java -Xmx2g -XX:MaxPermSize=512m -jar $$PICARDTOOLS_ROOT/CollectInsertSizeMetrics.jar \
	REFERENCE_SEQUENCE=$^2 \
	HISTOGRAM_FILE=$(basename $@).pdf \
	INPUT=$^3 \
	OUTPUT=$@ \
	HISTOGRAM_WIDTH=$(HISTOGRAM_WIDTH) \
	VALIDATION_STRINGENCY=SILENT \
	ASSUME_SORTED=false \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CollectInsertSizeMetrics.$@.log

%.sort.insert_size.pdf: %.sort.insert_size.txt
	touch $@

%.sort.per.chr.coverage: logs %.sort.bam
	$(call load_modules); \
	qaCompute -m $^2 $@ 2>&1 \
	| tee $</qaCompute.$@.log

# calcolate genome size, genome really coverad and coverage of genome really covered
%.sort.cov: logs $(INDEXES) %.sort.bam
	$(call load_modules); \
	coverage -f $^2 $^3 >$@


%.fastq.1: logs %.sort.bam
	$(call load_modules); \
	java -Xmx2g -XX:MaxPermSize=512m -jar $$PICARDTOOLS_ROOT/picard.jar \
	SamToFastq \
	INPUT=$^2 \
	FASTQ=$@ \
	SECOND_END_FASTQ=$(basename $@).2 \
	INCLUDE_NON_PF_READS=true \
	VALIDATION_STRINGENCY=SILENT \
	3>&1 1>&2 2>&3 \
	| tee $</picard-SamToFastq.$@.log

%.fastq.2: %.fastq.1
	touch $@

%.1.fastq.gz: %.fastq.1
	gzip -c <$< >$@

%.2.fastq.gz: %.fastq.2
	gzip -c <$< >$@


.PHONY: test
test:
	@echo $(RG)

ALL +=  fq.1.gz \
	fq.2.gz \
	aln.sort.bam \
	mates.sort.bam \
	paired.sort.bam \
	mates.1.fastq.gz mates.2.fastq.gz \
	paired.1.fastq.gz paired.2.fastq.gz \
	aln.sort.insert_size.txt \
	aln.sort.insert_size.pdf \
	mates.sort.insert_size.txt paired.sort.insert_size.txt \
	mates.sort.insert_size.pdf paired.sort.insert_size.pdf \
	paired.sort.cov mates.sort.cov \
	paired.sort.per.chr.coverage mates.sort.per.chr.coverage


INTERMEDIATE += aln.1.sai \
		aln.2.sai \
		aln.sam \
		paired.sam \
		mates.fastq.1 mates.fastq.2 \
		paired.fastq.1 paired.fastq.2

CLEAN += logs \
	 $(wildcard *.sort.*)